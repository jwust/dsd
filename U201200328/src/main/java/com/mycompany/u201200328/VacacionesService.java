/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.u201200328;

import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author JORGE
 */
@SuppressWarnings("restriction")
@WebService
public interface VacacionesService {
    public String generarCodigo (@WebParam(name = "fecha1") String fecha1,
                                 @WebParam(name = "fecha2") String fecha2, 
                                 @WebParam(name = "dias") int dias
                                );
}
