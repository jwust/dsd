<%-- 
    Document   : BuscarInfracciones
    Created on : 15/01/2013, 07:11:47 PM
    Author     : JORGE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html >
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Buscar Infracciones</title>
    </head>
    <body>
        <h1>Busqueda de Infracciones</h1>
        
        <div>
            <table>
                <tr>
                    <td>Placa</td>
                    <td><input id="placa" /><a href="BuscarPersonas.html"><img src="btn_buscar.gif"></img></a></td>
                </tr>
            </table>
            <fieldset>
            <table>
                <tr><td>&nbsp;</td></tr>
                
                <tr>
                    <td><strong>Placa:</strong> EDX-548</td>
                    <td><strong>Marca:</strong> Toyota</td>
                    <td><strong>Modelo:</strong> Yaris</td>
                    <td><strong>Año Fabricacion:</strong> 2010</td>
                </tr>
                
                <tr><td>&nbsp;</td></tr>
            </table>
            </fieldset>
            <br>
            <table border="1">
                
                <thead>
                    <tr><strong>
                        <td>Codigo</td>
                        <td>Infraccion</td>
                        <td>Fecha Infraccion</td>
                        <td>Lugar de Infraccion</td>
                        <td>Estado</td>
                        </strong>
                    </tr>
                    
                </thead>
                <tbody>
                    <tr>
                        <td>1234</td>
                        <td>Cruzo luz roja</td>
                        <td>10/10/2012</td>
                        <td>San Borja - Lima</td>
                        <td>Pendiente de Pago</td>
                    </tr>
                    
                </tbody>
            </table>
            
        </div>
    </body>
</html>
