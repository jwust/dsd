package com.mycompany.u201200328.service.test;

import com.mycompany.u201200328.VacacionesService;
import junit.framework.Assert;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class VacacionesServiceTest {

	
	private VacacionesService vacacionesService = null;
	
	public VacacionesServiceTest()
	{
		ApplicationContext context = new ClassPathXmlApplicationContext("/applicationContext.xml");
        
        this.vacacionesService = (VacacionesService)context.getBean("vacacionesServiceClient");
	}
	
	
	@Test
	public void testGenerarCodigo()
    {
		
		String codigo = vacacionesService.generarCodigo("15/01/2013", "20/01/2012", 5);
                System.out.println(codigo);
		Assert.assertEquals(4, Integer.parseInt(codigo),0);
		
    }
	
}
